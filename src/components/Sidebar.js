import React, { useState } from 'react';
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';
import HomeIcon from '@mui/icons-material/Home';
import LogoutIcon from '@mui/icons-material/Logout';
import AddBusinessOutlinedIcon from '@mui/icons-material/AddBusinessOutlined';
import KeyboardArrowDownOutlinedIcon from '@mui/icons-material/KeyboardArrowDownOutlined';
import KeyboardArrowUpOutlinedIcon from '@mui/icons-material/KeyboardArrowUpOutlined';
import MenuIcon from '@mui/icons-material/Menu';
import CloseIcon from '@mui/icons-material/Close';
import rev from './reveroad.png';
import NotificationsNoneIcon from '@mui/icons-material/NotificationsNone';
import pro from './img.jpg'
import './Sidebar.css';

function Sidebar() {
    const [isDropdownOpen, setDropdownOpen] = useState(false);
    const [isSidebarOpen, setSidebarOpen] = useState(false);

    const toggleDropdown = () => {
        setDropdownOpen(!isDropdownOpen);
    };

    const toggleSidebar = () => {
        setSidebarOpen(!isSidebarOpen);
    };

    return (
        <>
            <div className="con">
                <div className="hamburger" onClick={toggleSidebar}>
                    {isSidebarOpen ? <CloseIcon /> : <MenuIcon />}
                </div>
                <div className={`side ${isSidebarOpen ? 'open' : ''}`}>
                    <div className="top">
                        <a href="/"><img src={rev} alt="Logo" /></a>
                        <div className="line"></div>
                        <button className='btn bg1'>
                            <AddCircleOutlineIcon />
                            <span>New Report</span>
                        </button>
                        <button className='btn bg2'>
                            <HomeIcon />
                            <span>Home</span>
                        </button>
                        <ul>
                            <li>
                                <button onClick={toggleDropdown} className='btn bg3'>
                                    <AddBusinessOutlinedIcon /> Emergency
                                    {isDropdownOpen ? <KeyboardArrowUpOutlinedIcon /> : <KeyboardArrowDownOutlinedIcon />}
                                </button>
                                {isDropdownOpen && (
                                    <ul className="dropdown">
                                        <li>Traffic</li>
                                        <li>Accidents</li>
                                        <li>Ambulance Service</li>
                                    </ul>
                                )}
                            </li>
                        </ul>
                    </div>
                    <button className='btn lg'>
                        <LogoutIcon />
                        <span>Logout</span>
                    </button>
                </div>
                <div className='nav'>
                    <NotificationsNoneIcon/>
                    <img src={pro} />
                </div>
            </div>
        </>
    );
}

export default Sidebar;
