import logo from './logo.svg';
import './App.css';
import Sidebar from './components/Sidebar';
import Home from './components/Home';

function App() {
  return (
    <>
      <div className="main-container">
        <div className="container">
          <Sidebar/>
        </div>
        <Home/>
      </div>
    </>
  );
}

export default App;
